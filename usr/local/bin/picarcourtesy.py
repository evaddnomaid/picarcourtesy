#!/usr/bin/python
import pygame
from time import sleep
from time import time
import RPi.GPIO as GPIO

# Pins
# 20: Shutdown
#  3: Wake
# 18: I2S amplifier
# 19: I2S amplifier
# 21: I2S amplifier
#  4: Track selection
#  5: Track selection
#  6: Track selection
# 27: Activation
activationpin = 27 # controls if the music should play
switchlist = [4, 5, 6] # combined to produce 8 track options

musicdir = "/var/local/picarcourtesy/music/"

GPIO.setmode(GPIO.BCM)
for pin in switchlist:
	GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.setup(activationpin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(3, GPIO.IN)

music_on_q = False

songlist = ['Punchdrunk.mp3', 'DuelingBanjos.mp3', 'HaveYouEverBeenToOmaha.mp3', 'CherryCola.mp3', 'EighthWonder.mp3', 'Gibbous.mp3', 'Swingin.mp3', 'Punchdrunk.mp3']

def track_choice():
	choice = 0
	for i in range(len(switchlist)):
		if not GPIO.input(switchlist[i]):
			choice = choice + pow(2, i)
	return choice % len(songlist)

def music_on():
	global music_on_q
	# Use a track_choice of 0 to disable playing music
	print "track_choice(): ", track_choice()
	if not music_on_q and track_choice() != 0:
		pygame.mixer.music.load(musicdir + songlist[track_choice()])
		music_on_q = True
		pygame.mixer.music.play()
	
def music_off():
	global music_on_q
	if music_on_q:
		music_on_q = False
		pygame.mixer.music.stop()

pygame.mixer.init()
pygame.mixer.music.set_volume(0.1)
while True:
	print time()
	if GPIO.input(3):
		print "Pin 3 is true"
	else:
		print "Pin 3 is FALSE"
	for i in range(5):
		if GPIO.input(activationpin):
			music_on()
		else:
			music_off()
	GPIO.wait_for_edge(activationpin, GPIO.BOTH)

while pygame.mixer.music.get_busy() == True:
	pass

