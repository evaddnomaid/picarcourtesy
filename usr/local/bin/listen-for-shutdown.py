#!/usr/bin/env python

import RPi.GPIO as GPIO
import subprocess
from time import time
from time import sleep

GPIO.setmode(GPIO.BCM)
#GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#GPIO.wait_for_edge(20, GPIO.FALLING)
GPIO.setup(3, GPIO.IN)

timestamp = time()
while True:
	#print "infinite loop"
	sleep(9)
	if GPIO.input(3):
		currtime = time()
		#print "Pin 3 false - currtime is " + str(currtime) + " and timestamp is " + str(timestamp)
		if currtime - timestamp > (60 * 30):
			#print "Calling shutdown"
			subprocess.call(['shutdown', '-h', 'now'], shell=False)
	else:
		timestamp = time()
		#print "Pin 3 true " + str(timestamp)
