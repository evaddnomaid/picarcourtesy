#! /bin/sh

### BEGIN INIT INFO
# Provides:          picarcourtesy.py
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
### END INIT INFO

# If you want a command to always run, put it here

# Carry out specific functions when asked to by the system
case "$1" in
  start)
    echo "Starting picarcourtesy.py"
    /usr/local/bin/picarcourtesy.py &
    ;;
  stop)
    echo "Stopping picarcourtesy.py"
    pkill -f /usr/local/bin/picarcourtesy.py
    ;;
  *)
    echo "Usage: /etc/init.d/picarcourtesy.sh {start|stop}"
    exit 1
    ;;
esac

exit 0
